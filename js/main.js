"use strict";
$(function() {
    var accessibleModals =  {
    	config: {
    		classModal: '.modal', // clase contenedor 1 modal
    		targetDestModals: '#modals-wrapper', // contendedor de modales
    		targetPageContent: '.page' // clase contenedor general de contenido de la página
    	},

    	/**
    	 * Función punto de entrada
    	 * @return void
    	 */
    	init: function() {
    		var $modals = jQuery(this.config.classModal);

            if ($modals.length > 0){ // si hay modales en el DOM
                this.move();
                this.bindEvents();
            }
    	},
    	/**
    	 * Mueva todas los modales (DOM) al contenedor de modales (capa que está fuera del
    	 * contendido general de la página)
    	 * @return void
    	 */
    	move: function() {
        	var $modals = jQuery(this.config.classModal),
        	countModals,
            $modalsWrapper = jQuery(this.config.targetDestModals),
            $elementsFocus,
            countElementsFocus;

            countModals = $modals.length;
	        for (var i = 0, j = countModals; i < j; i++) {
	            jQuery($modals[i]).appendTo($modalsWrapper);
	        }
	    },
	    /**
	     * Enlazado de eventos
	     * @return void
	     */
	    bindEvents: function() {
	    	var $modals = jQuery(this.config.classModal),
	    	countModals,
            $wrapperPageContent = jQuery(this.config.targetPageContent),
            $elementsFocus = [],
            countElementsFocus = [];

            // cuando se muestra una modal
	        $modals.on('show.bs.modal', function() {
	            $wrapperPageContent.attr('aria-hidden', 'true');
	            jQuery(this).attr('aria-hidden', 'false');
	        });

	        // cuando se oculta(cierra) una modal
	        $modals.on('hide.bs.modal', function() {
	            $wrapperPageContent.attr('aria-hidden', 'false');
	            jQuery(this).attr('aria-hidden', 'true');
	        });

	        // hacer ciclico en la modal la navegación mediante teclado
			countModals = $modals.length;
	        for (var i = 0, j = countModals; i < j; i++) {
	           	$elementsFocus = jQuery("button, input, textarea, a", $modals[i]);
				countElementsFocus = $elementsFocus.length;
				// primero elemento focusable (la X de cerrar)
				(function($elementsFocusC, iC, countElementsFocusC) {
					$($elementsFocusC[0]).on("keydown",function(event) {
						if ((event.shiftKey) && ((event.which === 9) || (event.keyCode === 9))) {
							event.preventDefault();
							$($elementsFocusC[$elementsFocusC.length-1]).focus();
						}
					});
					// último elemento focusable de la modal
					$($elementsFocusC[countElementsFocusC-1]).on("keydown",function(event) {
						if ((!event.shiftKey) && ((event.which === 9) || (event.keyCode === 9))) {
							event.preventDefault();
							$($elementsFocusC[0]).focus();
						}
					});
				}($elementsFocus, i, countElementsFocus));
	        }
	    }
    };


	!function() {
		accessibleModals.init();
	}();
});